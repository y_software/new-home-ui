.PHONY: default
.PHONY: install
.PHONY: uninstall

INSTALL_DIR ?= /srv/http/new-home-ui

default:
	@echo "You have to specify install or uninstall"

install:
	mkdir -p "${INSTALL_DIR}"
	cp -R ./public "${INSTALL_DIR}"
