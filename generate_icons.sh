#!/bin/bash

convert -background transparent -density 500 -resize 144x144 public/icons/new-home.svg public/icons/new-home-144.png
convert -background transparent -density 500 -resize 192x192 public/icons/new-home.svg public/icons/new-home-192.png
convert -background transparent -density 500 -resize 512x512 public/icons/new-home.svg public/icons/new-home-512.png
