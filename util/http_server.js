/* eslint-disable */ // Eslint is not meant to check this util file

/*
 * Had to use a custom http server, which supports the "CALL" HTTP method,
 * which is used in the Rust part of New Home to call applications methods
 */

const nodeStatic = require('node-static');

//
// Create a node-static server instance to serve the './public' folder
//
const file = new nodeStatic.Server('./public', { cache: 0 });

require('http').createServer(function(request, response) {
    request.addListener('end', function() {
        //
        // Serve files!
        //
        file.serve(request, response);
    }).resume();
}).listen(8080);
