[![Pipeline Status](https://gitlab.com/Y_Software/new-home-ui/badges/master/pipeline.svg)](https://gitlab.com/Y_Software/new-home-ui/pipelines)

# New Home UI

This will become the UI of the new-home Software system, a central view to control your entire living space.
The New Home project consists of multiple components. The other components can be found here:
- [Core](https://gitlab.com/Y_Software/new-home-core)
- [Application Framework](https://gitlab.com/Y_Software/new-home-application)

## Features

| Features | Status | Note |
| --- | --- | --- |
| Initial setup/configuration window | done |
| General i18n/support for languages | planned |
| Adding rooms | done |
| Remove rooms | done |
| Adding devices | done |
| Modify devices | planned |
| Remove devices | done |
| Add applications | done |
| Modify applications | planned |
| Remove applications | planned | For this we need changes in the core, so if the application is used in a room/device it cannot be removed |

## Documentation

The documentation for the project is not yet available. The API might change in the future, but documentation will follow soon (tm).

## How to use

    # Run application
    npm start

    # Lint code
    npm run lint:eslint
