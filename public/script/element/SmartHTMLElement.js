import { Html } from '../functions/Html.js';

const FIRST_CHAR = 1;

export class SmartHTMLElement extends HTMLElement {

    /**
     * Registers the defined webcomponent
     *
     * @return {tagFunctionCallback<this>}
     */
    static register() {
        window.customElements.define(this.tagName, this);

        return (new Html(this.tagName)).getTagFunction();
    }

    /**
     * @returns {string}
     */
    static get tagName() {
        const className = this.name;
        const elementName = className.replace(
            /[A-Z]/g,
            (char) => `-${char.toLowerCase()}`,
        ).substr(FIRST_CHAR); // Substr to remove first "-"

        return elementName;
    }

    /**
     * native function, called when the component is connected to the dom
     */
    connectedCallback() {
        this._createShadow();
        this._setClasses();
    }

    /**
     * Gets the style for the component // Used during build
     *
     * @return {string}
     */
    getStyles() {
        return '';
    }

    /**
     * Gets the content for the web component // Used during build
     *
     * @return {Node|Node[]|Promise<Node|Node[]>}
     */
    getContent() {
        return [];
    }

    /**
     * Gets classes which should be set on the content container based on the attributes // Default []
     *
     * @return {string[]}
     */
    getContentModifier() {
        return [];
    }

    /**
     * Gets classes which should be set based on the attribute // Default ['flex', 'inline', 'grid']
     *
     * @return {string[]}
     */
    getComponentModifier() {
        return ['flex', 'inline', 'grid'];
    }

    /**
     * Gets the tag that should be used for the content // Used during build // Default: 'div'
     *
     * @return {string}
     */
    getContentElementTag() {
        return 'div';
    }

    /**
     * Returns a css variable with a given value from the "data-" attributes
     * Uses a --*class-name*--name variable when defined by parent. Otherwise uses the fallback.
     *
     * @param {string} name The name of the css variable used internally
     * @param {string} fallback The fallback value for the css variable
     */
    getCssVariableFromAttribute(name, fallback) {
        const dataName = this._hyphenToCamelCase(name);
        let value = `var(--${this.tagName.toLowerCase()}--${name}, ${fallback})`;

        if (this.dataset[dataName]) {
            value = this.dataset[dataName];
        }

        return `--${name}: ${value};`;
    }

    /**
     * Gets the given style if the given attribute is set on the element
     *
     * @param {string} attributeName name of the attribute that has to be set
     * @param {string} style the style that should be applied
     * @param {string} elseStyle the style that should be applied if the attribute is not set
     *
     * @return {string}
     */
    setConditionalStyle(attributeName, style, elseStyle = '') {
        if (this.hasAttribute(attributeName)) {
            return style;
        }

        return elseStyle;
    }

    /**
     * Dispatches a new `CustomEvent` so that it bubbles through the whole DOM tree (if its not cancelled)
     *
     * @param {string} eventName the name of the custom event
     * @param {object} detail The custom event's data
     */
    dispatchCustomEvent(eventName, detail) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail }));
    }

    /**
     * Sets custom elements classes and display modifiers
     *
     * @private
     *
     * @return {void}
     */
    _setClasses() {
        this.classList.add('custom-element');

        for (const attribute of this.getComponentModifier()) {
            this._setAttributeClass(attribute);
        }
    }

    /**
     * Sets a modifier class
     *
     * @private
     *
     * @param {string} name The name of the attribute to check
     *
     * @return {void}
     */
    _setAttributeClass(name) {
        if (this.hasAttribute(name)) {
            this.classList.add(`${name}-element`);
        }
    }

    /**
     * Initializes the shadow dom
     *
     * @private
     *
     * @return {void}
     */
    async _createShadow() {
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(this._createStyle());
        this.shadowRoot.appendChild(await this._createContent());
    }

    /**
     * Creates the content element
     *
     * @private
     *
     * @return {HTMLDivElement}
     */
    async _createContent() {
        this.content = document.createElement(this.getContentElementTag());
        this.content.classList.add(this.tagName.toLowerCase());

        const elementContent = await this.getContent();
        this._addChildNodes(this.content, elementContent);

        for (const className of this.getContentModifier()) {
            if (this.hasAttribute(className)) {
                this.content.classList.add(`is--${className}`);
            }
        }

        return this.content;
    }

    /**
     * @param {HTMLElement} contentNode
     * @param {HTMLElement[]|HTMLElement} elementContent
     *
     * @private
     */
    _addChildNodes(contentNode, elementContent) {
        if (Array.isArray(elementContent)) {
            for (const element of elementContent) {
                contentNode.appendChild(element);
            }

            return;
        }

        contentNode.appendChild(elementContent);
    }

    /**
     * Creates the style element
     *
     * @private
     *
     * @return {HTMLStyleElement}
     */
    _createStyle() {
        const style = document.createElement('style');
        style.innerHTML = this.getStyles();

        return style;
    }

    /**
     * @param {string} text The string which should be converted
     *
     * @return {string}
     */
    _hyphenToCamelCase(text) {
        return text.replace(/-([a-z])/g, (char) => char[FIRST_CHAR].toUpperCase());
    }
}
