import { MessageBuilder } from '../service/MessageBuilder.js';
import { SmartButtonTag } from './SmartButton.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';

export class SmartMessage extends SmartHTMLElement {
    constructor() {
        super();

        this.messageBuilder = new MessageBuilder();
    }

    /**
     * @returns {string}
     */
    getStyles() {
        return `${this.messageBuilder.getStyle()}

        .${this.tagName.toLowerCase()} {
            background-color: var(--colors--background--floating-dark);
            display: flex;
            margin: 0 auto;
            max-width: 768px;
            justify-content: space-between;
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        
        ::slotted(*) {
            margin: 0.5em 0;
        }
        `;
    }

    /**
     * @returns {string}
     */
    getContentElementTag() {
        return 'div';
    }

    /**
     * @returns {HTMLElement[]}
     */
    getContent() {
        const dismiss = SmartButtonTag.link`Dismiss`;
        dismiss.addEventListener('click', this._onClickDismiss.bind(this));

        return [
            ...this.messageBuilder.buildMessage(this._getMessage()),
            dismiss,
        ];
    }

    _onClickDismiss() {
        this.dispatchCustomEvent('SmartMessage.dismiss', {});
    }

    /**
     * @returns {
     * ({error: string}|
     * {message: string}|
     * {markdown: string}|
     * {success: string}|
     * {debug: string}|
     * {info: string}|
     * {list: string}|
     * {table: string})
     * }
     *
     * @private
     */
    _getMessage() {
        if (this.message) {
            return this.message;
        }

        return {
            error: 'No message provided!',
        };
    }
}

SmartMessage.register();
