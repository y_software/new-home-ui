import { SmartHTMLElement } from './SmartHTMLElement.js';
import { SmartMessage } from './SmartMessage.js';

const MESSAGE_SHOW_DURATION = 5000;
const MESSAGE_ANIMATION_DURATION = 200;
const ARRAY_EMPTY_LENGTH = 0;
const PERCENT_MULTIPLIER = 100;

export class SmartMessageContainer extends SmartHTMLElement {
    /**
     * @type {SmartMessage[]}
     */
    messageQueue = [];

    queueRunning = false;

    constructor() {
        super();

        this.addEventListener('smartMessage', this._onSmartMessage.bind(this));
    }

    /* eslint-disable */

    getStyles() {
        const VISIBLE_AT = (MESSAGE_ANIMATION_DURATION / MESSAGE_SHOW_DURATION) * PERCENT_MULTIPLIER;

        return `
            .${this.tagName.toLowerCase()} {
                bottom: 0;
                left: 0;
                position: fixed;
                width: 100%;
            }
            
            smart-message {
                animation: smart-message-fade-in-out ${MESSAGE_SHOW_DURATION}ms;
                bottom: 0;
                position: absolute;
                width: 100%;
            }
            
            smart-message.hide {
                animation: smart-message-fade-out ${MESSAGE_ANIMATION_DURATION}ms;
            }
            
            @keyframes smart-message-fade-in-out {
                0% {
                    transform: translateY(100%);
                }
            
                ${VISIBLE_AT}% {
                    transform: translateY(0);
                }
            
                ${PERCENT_MULTIPLIER - VISIBLE_AT}% {
                    transform: translateY(0);
                }
                
                100% {
                    transform: translateY(100%);
                }
            }

            @keyframes smart-message-fade-out {
                0% {
                    transform: translateY(0);
                }
            
                100% {
                    transform: translateY(100%);
                }
            }
        `;
    }

    /* eslint-enable */

    /**
     * @param {object[]} messageObjects
     */
    addMessages(messageObjects) {
        if (!Array.isArray(messageObjects)) {
            this.addMessage(messageObjects);

            return;
        }

        messageObjects = [].concat(...messageObjects.map(this._flattenMessageObject.bind(this)));

        for (const message of messageObjects) {
            this.addMessage(message);
        }
    }

    /**
     * @param {object} messageObject
     */
    addMessage(messageObject) {
        for (const message of this._flattenMessageObject(messageObject)) {
            this._queueMessage(this._buildSmartMessage(message));
        }
    }

    /**
     * @param {{messages: Array}} detail
     *
     * @private
     */
    _onSmartMessage({ detail }) {
        this.addMessages(detail.messages);
    }

    /**
     * @param {object} messageObject
     * @returns {object[]}
     *
     * @private
     */
    _flattenMessageObject(messageObject) {
        const messages = [];

        for (const [type, content] of Object.entries(messageObject || {})) {
            messages.push({ [type]: content });
        }

        return messages;
    }

    /**
     * @param {object} messageObject
     * @returns {SmartMessage}
     *
     * @private
     */
    _buildSmartMessage(messageObject) {
        const messageElement = document.createElement(SmartMessage.tagName);
        messageElement.message = messageObject;

        return messageElement;
    }

    /**
     * @param {SmartMessage} message
     *
     * @private
     */
    _queueMessage(message) {
        this.messageQueue.push(message);

        this._runQueue();
    }

    /**
     * @private
     */
    async _runQueue() {
        if (this.queueRunning) {
            return;
        }

        this.queueRunning = true;

        while (this.messageQueue.length > ARRAY_EMPTY_LENGTH) {
            await this._showMessage(this.messageQueue.shift());
        }

        this.queueRunning = false;
    }

    /**
     * @param {SmartMessage} message
     * @returns {Promise<void>}
     *
     * @private
     */
    _showMessage(message) {
        return new Promise(this._showMessagePromise.bind(this, message));
    }

    /**
     * @param {SmartMessage} message
     * @param {function} res
     *
     * @private
     */
    _showMessagePromise(message, res) {
        this.content.appendChild(message);
        message.classList.add('shown');

        const finishTimeout = setTimeout(this._clearMessage.bind(this, res, message), MESSAGE_SHOW_DURATION);
        message.addEventListener('SmartMessage.dismiss', this._cancelMessage.bind(this, finishTimeout, res, message));
    }

    /**
     * @param {function} res
     * @param {SmartMessage} message
     *
     * @private
     */
    _clearMessage(res, message) {
        res();
        message.remove();
    }

    /**
     * @param {number} animationTimeout
     * @param {SmartMessage} message
     * @param {function} res
     *
     * @private
     */
    _cancelMessage(animationTimeout, res, message) {
        clearTimeout(animationTimeout);
        message.classList.add('hide');

        setTimeout(this._clearMessage.bind(this, res, message), MESSAGE_ANIMATION_DURATION);
    }
}

export const SmartMessageContainerTag = SmartMessageContainer.register();
