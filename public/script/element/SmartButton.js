import { Html } from '../functions/Html.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';

export class SmartButton extends SmartHTMLElement {
    /**
     * @returns {string}
     */
    getContentElementTag() {
        return 'button';
    }

    /* eslint-disable */

    /**
     * @returns {string}
     */
    getStyles() {
        let type = 'neutral';

        if (this.hasAttribute('type')) {
            type = this.getAttribute('type');
        }

        return `
        .${this.tagName.toLowerCase()} {
            ${this.getCssVariableFromAttribute('accent-color', `var(--colors--message--${type})`)}
            ${this.getCssVariableFromAttribute('hover-text', 'var(--colors--text--normal)')}
            ${this.getCssVariableFromAttribute('background-color', 'var(--colors--background--floating)')}

            ${this.setConditionalStyle('thin', `
                --v-padding: 0.375em;
            `, `
                --v-padding: 4em;
            `)}
            
            ${this.setConditionalStyle('small', `
                width: auto;
            `, `
                width: 100%;
            `)}

            ${this.setConditionalStyle('transparent', `
                --background-color: transparent;
                --hover-background: rgba(0, 0, 0, 0.3);
            `, `
                --hover-background: var(--accent-color);
            `)}

            ${this.setConditionalStyle('uppercase', `
                text-transform: uppercase;
            `)}

            background: var(--background-color);
            border: 0 none;
            border-radius: 2px;
            color: var(--accent-color);
            cursor: pointer;
            font-size: 1em;
            font-family: 'Roboto';
            height: 100%;
            outline: none !important;
            padding: var(--v-padding) 1.5em;
            text-align: center;
            transition: background 0.1s ease-in, color 0.1s ease-in;
            line-height: 1.125em;
        }

        .${this.tagName.toLowerCase()}:hover {
            background: var(--hover-background);
            color: var(--hover-text);
        }
        `;
    }

    /* eslint-enable */

    /**
     * @returns {Promise<HTMLElement>}
     */
    async getContent() {
        return document.createElement('slot');
    }
}

export const SmartButtonTag = SmartButton.register();

SmartButtonTag.small = Html.addModifier(['thin', 'small']);
SmartButtonTag.link = Html.addModifier(['thin', 'small', 'transparent', 'uppercase', 'no-hover']);
