import { Html } from '../functions/Html.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';

export class SmartTitle extends SmartHTMLElement {
    getContentElementTag() {
        return 'h1';
    }

    getStyles() {
        return `
            .${this.tagName.toLowerCase()} {
                ${this.getCssVariableFromAttribute('text-color', '#FFF')}

                color: var(--text-color);
                font-size: 1.5em;
                flex: 1 1 auto;
                margin: 0;
                
                ${this.setConditionalStyle('center', `
                text-align: center;
                `)}
            }
        `;
    }

    getContent() {
        return document.createElement('slot');
    }
}

export const SmartTitleTag = SmartTitle.register();

SmartTitleTag.center = Html.addModifier('center');
