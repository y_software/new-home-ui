import { SmartHTMLElement } from './SmartHTMLElement.js';

export class GridContainer extends SmartHTMLElement {
    /**
     * @returns {string}
     */
    getContentElementTag() {
        return 'slot';
    }

    /**
     * @returns {string}
     */
    getStyles() {
        return `
            .${this.tagName.toLowerCase()} {
                ${this.getCssVariableFromAttribute('inner-padding', '1em')}
                ${this.getCssVariableFromAttribute('width', '80%')}
                ${this.getCssVariableFromAttribute('min-width', '320px')}
            
                box-sizing: border-box;
                display: flex;
                flex-flow: row wrap;
                margin: 0 auto;
                overflow: hidden;
                padding: var(--inner-padding);
                width: var(--width);
                min-width: var(--min-width);
                
                ${this.setConditionalStyle('v-align', `
                    align-items: center;
                `)}
                
                ${this.setConditionalStyle('column', `
                    flex-direction: column;
                `)}
            }

            ::slotted(*) {
                flex: 1 1 30%;
                margin: 0.5em;
            }

            ::slotted(.${this.tagName.toLowerCase()}--full-width) {
                flex: 1 1 100%;
            }
            
            @media (max-width: 1024px) {
                ::slotted(*) {
                    flex: 1 1 40%;
                    margin: 0.5em;
                }
            }
            
            @media (max-width: 768px) {
                ::slotted(*) {
                    flex: 1 1 100%;
                    margin: 0.5em 0;
                }
            }
        `;
    }

    /**
     * @returns {HTMLElement[]}
     */
    getContent() {
        return [];
    }
}

export const GridContainerTag = GridContainer.register();
