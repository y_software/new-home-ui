import { Li } from '../functions/Html.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';

export class SmartNavigation extends SmartHTMLElement {
    constructor() {
        super();

        this.list = document.createElement('ul');
        this.addEventListener('smartNavigationAddItem', this._onAddItem.bind(this));
    }

    /**
     * @returns {string}
     */
    getContentElementTag() {
        return 'nav';
    }

    /**
     * @returns {string}
     */
    getStyles() {
        return `
            .${this.tagName.toLowerCase()} {
                ${this.getCssVariableFromAttribute('hover-color', '#FFF')}
                ${this.getCssVariableFromAttribute('text-color', '#DDD')}
            }

            ul, li {
                list-style: none;
                margin: 0;
                display: flex;
                padding: 0;
            }
            
            li {
                border-bottom: solid 2px var(--text-color); 
                cursor: pointer;
                color: var(--text-color);
                font-size: 1.1em;
                font-weight: bold;
                padding: 0.5em 1em;
                margin: 0 0.2em;
            }

            li:hover {
                --text-color: var(--hover-color);
            }
            
            p {
                margin: 0;
            }

            @media (max-width: 768px) {
                li {
                    font-size: 1em;
                    padding: 0.5em;
                }
            }
        `;
    }

    /**
     * @returns {HTMLUListElement}
     */
    getContent() {
        return this.list;
    }

    /**
     * @param {NavigationItem} navigationItem
     */
    addNavigationItem({ title, onClick }) {
        const element = Li`<p>${title}</p>`;
        element.addEventListener('click', onClick.bind(this));

        this.list.appendChild(element);
    }

    /**
     * @param {{item: NavigationItem}} detail
     *
     * @private
     */
    _onAddItem({ detail }) {
        const item = detail.item;

        if (item.onClick && item.title) {
            this.addNavigationItem(item);
        }
    }
}

SmartNavigation.register();
