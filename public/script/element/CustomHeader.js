import { GridContainerTag } from './GridContainer.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';
import { SmartNavigation } from './SmartNavigation.js';
import { SmartTitleTag } from './SmartTitle.js';

export class CustomHeader extends SmartHTMLElement {
    /**
     * @returns {string}
     */
    getContentElementTag() {
        return 'header';
    }

    /**
     * @returns {string}
     */
    getStyles() {
        return `
            .${this.tagName.toLowerCase()} {
                ${this.getCssVariableFromAttribute('background-color', '#12AA8A')}
                ${this.getCssVariableFromAttribute('text-color', '#FFF')}

                background: var(--background-color);
                color: var(--text-color);
                margin-bottom: 1em;
            }

            smart-navigation {
                flex: 0 1 auto;
                margin-bottom: 0;
            }
            
            smart-title {
                margin: 0;
            }

            @media (max-width: 1024px) {
                .${this.tagName.toLowerCase()} {
                    font-size: 1em;
                    --grid-container--width: 100%;
                }
            }
        `;
    }

    /**
     * @returns {GridContainer}
     */
    getContent() {
        const navigation = document.createElement(SmartNavigation.tagName);
        const gridContainer = GridContainerTag`${SmartTitleTag`<slot>`}${navigation}`;

        window.mainNavigation = navigation;

        gridContainer.setAttribute('v-align', '');
        gridContainer.setAttribute('column', '');
        gridContainer.setAttribute('data-inner-padding', '0.375em 1em');

        return gridContainer;
    }
}

CustomHeader.register();
