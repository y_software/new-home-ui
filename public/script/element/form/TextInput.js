import { Label } from '../../functions/Html.js';
import { SmartFormElement } from './SmartFormElement.js';

export class TextInput extends SmartFormElement {
    input = document.createElement('input');

    get value() {
        return this.input.value;
    }

    set value(value) {
        this.input.value = value;
    }

    getStyles() {
        return `
            label {
                color: var(--colors--text--normal);
            }
            
            input {
                ${this.getCssVariableFromAttribute('border-color', 'var(--colors--background--floating)')}

                background-color: var(--colors--background--input);
                border-radius: 2px;
                border: 0 none;
                border-bottom: var(--border-color) solid 2px;
                box-sizing: border-box;
                font-size: 1em;
                outline: none !important;
                padding: 0.3125em;
                width: 100%;
            }
        `;
    }

    getContent() {
        this.input.type = 'text';
        this.input.name = this.name;

        this.input.addEventListener('keyup', this.dispatchCustomEvent.bind(this, 'SmartFormElement.update', {}));

        return [Label`<slot></slot> ${this.input}`];
    }
}

const TagFunction = TextInput.register();

/**
 * @param {string} name
 * @returns {tagFunctionCallback<TextInput>}
 */
export function TextInputTag(name) {
    return function TextInputTagInner(...content) {
        const element = TagFunction(...content);

        element.name = name;

        return element;
    };
}
