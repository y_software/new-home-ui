import { Label } from '../../functions/Html.js';
import { SmartFormElement } from './SmartFormElement.js';

export class SelectInput extends SmartFormElement {
    /**
     * @type {HTMLSelectElement}
     */
    select = document.createElement('select');

    /**
     * @type {[string]}
     */
    options = [];

    _value = '';

    constructor() {
        super();

        this.select.addEventListener('change', this.dispatchCustomEvent.bind(this, 'SmartFormElement.update', {}));
    }

    connectedCallback() {
        super.connectedCallback();

        this.select.value = this._value;
    }

    get value() {
        return this.select.value || this._value;
    }

    set value(value) {
        this.select.value = value;
        this._value = value;
        this.select.dispatchEvent(new Event('change'));
    }

    getStyles() {
        return `
            label {
                color: var(--colors--text--normal);
            }

            select {
                ${this.getCssVariableFromAttribute('border-color', 'var(--colors--background--floating)')}

                background-color: var(--colors--background--input);
                border-radius: 2px;
                border: 0 none;
                border-bottom: var(--border-color) solid 2px;
                font-size: 1em;
                outline: none !important;
                padding: 0.3125em;
                width: 100%;
            }
        `;
    }

    getContent() {
        this.select.name = this.name;
        this._insertSelectOptions(this.options);

        return [Label`<slot></slot> ${this.select}`];
    }

    /**
     * @param {[string]} options
     * @private
     */
    _insertSelectOptions(options) {
        for (const option of options) {
            const optionElement = document.createElement('option');
            optionElement.innerHTML = option;
            optionElement.value = option;

            this.select.appendChild(optionElement);
        }
    }
}

const TagFunction = SelectInput.register();

/**
 * @param {string} name
 * @param {[string]} options
 * @returns {tagFunctionCallback<SelectInput>}
 */
export function SelectInputTag(name, options) {
    return function SelectInputTagInner(...content) {
        const element = TagFunction(...content);

        element.name = name;
        element.options = options;

        return element;
    };
}
