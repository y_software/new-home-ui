const HSL_MAX_HUE = 360;

/* eslint max-lines-per-function: 0 */
/**
 * @this {ColorInput}
 *
 * @returns {string}
 */
export function getStyles() {
    return `
            .${this.tagName.toLowerCase()} {
                ${this.getCssVariableFromAttribute('color', 'var(--colors--text--normal)')}
                ${this.getCssVariableFromAttribute('accent', 'var(--colors--background--floating-active)')}
            }

            label {
                color: var(--color);
            }

            .color-picker {
                padding: 0.625em;
            }

            .bar {
                border-radius: 0.3125em;
                height: 1.25em;
                position: relative;
                width: 100%;
            }

            .bar:not(:last-of-type) {
                margin-bottom: 0.625em;
            }

            .bar--hue {
                background: linear-gradient(90deg, #F00, #FF0, #0F0, #0FF, #00F, #F0F, #F00);
            }

            .bar--saturation {
                background: linear-gradient(90deg, hsl(var(--hue), 0%, 50%), hsl(var(--hue), 100%, 50%));
            }

            .bar--lightness {
                background: linear-gradient(
                    90deg,
                    hsl(var(--hue), 100%, 0%),
                    hsl(var(--hue), 100%, 50%),
                    hsl(var(--hue), 100%, 100%)
                );
            }

            hr {
                border: 1px solid var(--color);
                margin: 0.625em 0;
            }

            .bar--result {
                background: hsl(var(--hue), var(--saturation), var(--lightness));
            }

            .bar--hue .color--selector {
                left: calc((var(--hue) / ${HSL_MAX_HUE}) * 100%);
            }

            .bar--saturation .color--selector {
                left: var(--saturation);
            }

            .bar--lightness .color--selector {
                left: var(--lightness);
            }

            .color--selector {
                height: 1em;
                position: absolute;
                top: 0.125em;
                width: 1px;
            }

            .color--selector::before {
                border: 2px var(--accent) solid;
                box-sizing: border-box;
                content: '';
                display: block;
                height: 1.625em;
                left: -0.3125em;
                position: absolute;
                top: -5px;
                width: 0.6875em;
            }
        `;
}
