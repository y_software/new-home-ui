import { SmartHTMLElement } from '../SmartHTMLElement.js';

export class SmartFormElement extends SmartHTMLElement {
    name = '';

    /**
     * @returns {string|boolean|number}
     */
    get value() {
        throw new Error('Value getter is not implemented!');
    }

    /**
     * @param {string|boolean|number} value
     */
    set value(value) {
        throw new Error('Value setter is not implemented!');
    }
}
