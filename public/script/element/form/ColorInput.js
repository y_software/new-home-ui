import { Div } from '../../functions/Html.js';
import { SmartFormElement } from './SmartFormElement.js';
import { getStyles } from './ColorInput.style.js';

const MIN_PERCENT = 0;
const MAX_PERCENT = 1;
const PERCENT_PRECISION = 10000;
const PERCENT_MULTIPLIER = 100;
const HSL_MAX_HUE = 360;
const HSL_PRE_CUTOFF = 4;
const HSL_POST_CUTOFF = 1;
const BAR_LEFT_OFFSET_FALLBACK = 0;
const PREFERED_TOUCH_FINGER = 0;
const MOUSE_X_FALLBACK = 0;

export class ColorInput extends SmartFormElement {
    constructor() {
        super();

        this.getStyles = getStyles.bind(this);
    }

    get value() {
        const hue = this.content.style.getPropertyValue('--hue');
        const saturation = this.content.style.getPropertyValue('--saturation');
        const lightness = this.content.style.getPropertyValue('--lightness');

        return `hsl(${hue}, ${saturation}, ${lightness})`;
    }

    set value(value) {
        if (!value.startsWith('hsl')) {
            throw new Error('Given value is not HSL!');
        }

        value = value.replace(/ /g, '');
        value = value.substr(HSL_PRE_CUTOFF, value.length - HSL_PRE_CUTOFF + HSL_POST_CUTOFF);
        const [hue, saturation, lightness] = value.split(',');

        this.content.style.setProperty('--hue', hue);
        this.content.style.setProperty('--saturation', saturation);
        this.content.style.setProperty('--lightness', lightness);
    }

    getContent() {
        this.content.style.setProperty('--hue', '36');
        this.content.style.setProperty('--saturation', '100%');
        this.content.style.setProperty('--lightness', '60%');

        return [
            Div`
                <label><slot/></label>

                <div class="color-picker">
                    ${this._makeBar('hue', this._updateHue.bind(this))}
                    ${this._makeBar('saturation', this._updateSaturation.bind(this))}
                    ${this._makeBar('lightness', this._updateLightness.bind(this))}
                    <hr>
                    <div class="bar bar--result"></div>
                </div>
            `,
        ];
    }

    /**
     * @param name
     * @param {function} updateCallback
     *
     * @private
     */
    _makeBar(name, updateCallback) {
        const bar = Div`<div class="color--selector"></div>`;
        bar.classList.add('bar');
        bar.classList.add(`bar--${name}`);
        bar.addEventListener(
            'mousedown',
            this._enableDragEvent.bind(this, updateCallback.bind(this, bar)),
        );
        bar.addEventListener(
            'touchstart',
            this._enableDragEvent.bind(this, updateCallback.bind(this, bar)),
            { passive: true },
        );

        return bar;
    }

    /**
     * @param {function} eventCallback
     * @param {MouseEvent} event
     */
    _enableDragEvent(eventCallback, event) {
        event.preventDefault();

        eventCallback(event);

        this.content.addEventListener(
            'mouseup',
            this._disableDragEvent.bind(this, this.content, eventCallback),
            { once: true },
        );
        this.content.addEventListener(
            'mouseleave',
            this._disableDragEvent.bind(this, this.content, eventCallback),
            { once: true },
        );
        this.content.addEventListener(
            'touchend',
            this._disableDragEvent.bind(this, this.content, eventCallback),
            { once: true },
        );
        this.content.addEventListener('mousemove', eventCallback);
        this.content.addEventListener('touchmove', eventCallback, { passive: true });
    }

    /**
     * @param {HTMLElement} element
     * @param {function} eventCallback
     */
    _disableDragEvent(element, eventCallback) {
        element.removeEventListener('mousemove', eventCallback);
        element.removeEventListener('touchmove', eventCallback);
    }

    /**
     * @param {HTMLElement} bar
     * @param {MouseEvent} event
     */
    _updateHue(bar, event) {
        this.content.style.setProperty(
            '--hue',
            (this._calculatePercentage(bar, event) * HSL_MAX_HUE).toString(),
        );

        this._triggerUpdate();
    }

    /**
     * @param {HTMLElement} bar
     * @param {MouseEvent|TouchEvent} event
     *
     * @returns {number}
     */
    _calculatePercentage(bar, event) {
        const barLeft = bar.getBoundingClientRect().x || BAR_LEFT_OFFSET_FALLBACK;
        const mouseOffset = this._getEventX(event) - barLeft;
        const percent = mouseOffset / bar.clientWidth;

        if (percent < MIN_PERCENT) {
            return MIN_PERCENT;
        }

        if (percent > MAX_PERCENT) {
            return MAX_PERCENT;
        }

        return Math.round(percent * PERCENT_PRECISION) / PERCENT_PRECISION;
    }

    /**
     * @param {MouseEvent|TouchEvent} event
     * @returns {number}
     * @private
     */
    _getEventX(event) {
        if (event instanceof MouseEvent) {
            return event.x;
        }

        if (event instanceof TouchEvent) {
            return event.touches[PREFERED_TOUCH_FINGER].clientX;
        }

        return MOUSE_X_FALLBACK;
    }

    /**
     * @param {HTMLElement} bar
     * @param {MouseEvent} event
     */
    _updateSaturation(bar, event) {
        this.content.style.setProperty(
            '--saturation',
            `${(this._calculatePercentage(bar, event) * PERCENT_MULTIPLIER).toString()}%`,
        );

        this._triggerUpdate();
    }

    /**
     * @param {HTMLElement} bar
     * @param {MouseEvent} event
     */
    _updateLightness(bar, event) {
        this.content.style.setProperty(
            '--lightness',
            `${(this._calculatePercentage(bar, event) * PERCENT_MULTIPLIER).toString()}%`,
        );

        this._triggerUpdate();
    }

    /**
     * @private
     */
    _triggerUpdate() {
        this.dispatchCustomEvent('SmartFormElement.update', {});
    }
}

const TagFunction = ColorInput.register();

/**
 * @param {string} name
 * @returns {tagFunctionCallback<ColorInput>}
 */
export function ColorInputTag(name) {
    return function ColorInputTagInner(...content) {
        const element = TagFunction(...content);

        element.name = name;

        return element;
    };
}
