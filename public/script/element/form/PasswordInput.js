import { TextInput } from './TextInput.js';

export class PasswordInput extends TextInput {

    getContent() {
        const content = super.getContent();

        this.input.type = 'password';

        return content;
    }
}

const TagFunction = PasswordInput.register();

/**
 * @param {string} name
 * @returns {tagFunctionCallback<PasswordInput>}
 */
export function PasswordInputTag(name) {
    return function PasswordInputTagInner(...content) {
        const element = TagFunction(...content);

        element.name = name;

        return element;
    };
}
