import { Html } from '../../functions/Html.js';
import { SmartHTMLElement } from '../SmartHTMLElement.js';

export class SmartForm extends SmartHTMLElement {
    /**
     * @type {SmartFormElement[]}
     */
    inputs = [];

    formWrapper = document.createElement('div');

    getStyles() {
        return `
            .form--wrapper * {
                display: block;
                margin-bottom: 1em;
            }
        `;
    }

    getContent() {
        this.formWrapper.classList.add('form--wrapper');

        return [this.formWrapper];
    }

    /**
     * @returns {{[string]: string}}
     */
    getData() {
        const data = {};

        for (const input of this.inputs) {
            data[input.name] = input.value;
        }

        return data;
    }

    /**
     * @param {SmartFormElement} element
     */
    addInput(element) {
        this.inputs.push(element);
        this.formWrapper.appendChild(element);

        element.addEventListener(
            'SmartFormElement.update',
            this.dispatchCustomEvent.bind(
                this,
                'SmartForm.update',
                { getData: this.getData.bind(this) },
            ),
        );
    }
}

export const SmartFormTag = SmartForm.register();

SmartFormTag.options = Html.supportsOptions({
    ...Html.defaultOptions,

    /**
     * @param {SmartForm} element
     * @param {[SmartFormElement]} inputs
     */
    inputs(element, inputs) {
        for (const input of inputs) {
            element.addInput(input);
        }
    },
});
