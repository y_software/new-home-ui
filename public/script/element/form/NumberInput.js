import { TextInput } from './TextInput.js';

const NAN_FALLBACK = 0;

export class NumberInput extends TextInput {
    get value() {
        let value = super.value;

        value = parseInt(value, 10);

        if (isNaN(value)) {
            value = NAN_FALLBACK;
        }

        if (Number.isInteger(this.input.min) && value < this.input.min) {
            value = this.input.min;
        }

        if (Number.isInteger(this.input.max) && value > this.input.max) {
            value = this.input.max;
        }

        return value;
    }

    getContent() {
        const content = super.getContent();
        this.input.type = 'number';

        return content;
    }
}

const TagFunction = NumberInput.register();

/**
 * @param {string} name
 * @param options
 * @returns {tagFunctionCallback<NumberInput>}
 */
export function NumberInputTag(name, options = {}) {
    return function NumberInputTagInner(...content) {
        const element = TagFunction(...content);

        for (const [propertyName, value] of Object.entries(options)) {
            element.input[propertyName] = value;
        }

        element.name = name;

        return element;
    };
}
