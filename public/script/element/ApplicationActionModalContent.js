import { ApiGateway } from '../service/ApiGateway.js';
import { SmartForm } from './form/SmartForm.js';
import { SmartHTMLElement } from '/script/element/SmartHTMLElement.js';
import { loadApplicationMessages } from '../functions/deviceActions/loadApplicationMessages.js';
import { loadApplicationModal } from '../functions/deviceActions/loadApplicationModal.js';
import { loadApplicationScripts } from '../functions/deviceActions/loadApplicationScripts.js';
import { loadApplicationView } from '../functions/deviceActions/loadApplicationView.js';

const RESPONSE_HANDLER = [
    loadApplicationScripts,
    loadApplicationView,
    loadApplicationModal,
    loadApplicationMessages,
];

export class ApplicationActionModalContent extends SmartHTMLElement {
    apiGateway = new ApiGateway();

    application = '';

    channel = '';

    submitAction = '';

    /**
     * @type {SmartForm}
     */
    form = document.createElement(SmartForm.tagName);

    /**
     * @type {ApplicationWindow}
     */
    window;

    /**
     * @returns {SmartFormElement[]}
     */
    getInputs() {
        return [];
    }

    getContent() {
        this.parentElement.addEventListener('SmartModal.submit', this._onModalSubmit.bind(this));

        for (const input of this.getInputs()) {
            this.form.addInput(input);
        }

        return [this.form];
    }

    /**
     * @param {object} event
     * @param {object} event.detail
     * @param {function} event.detail.closeModal
     * @returns {Promise<void>}
     * @private
     */
    async _onModalSubmit(event) {
        if (!this.window) {
            throw new Error(`Property window is missing in ${this.constructor.name}`);
        }

        try {
            const response = await this._callDeviceAction();

            for (const handler of RESPONSE_HANDLER) {
                await handler.call(this.window, response, this.application);
            }
        } catch (e) {
            if (this.window) {
                this.window.dispatchCustomEvent('smartMessage', { messages: e });
            }
        }

        event.detail.closeModal();
    }

    /**
     * @returns {Promise<object>}
     *
     * @private
     */
    async _callDeviceAction() {
        const response = await this.apiGateway.callMethod(this.application, this.submitAction, {
            channel: this.channel,
            ...this.form.getData(),
        });

        if (!response.success && !response.response) {
            throw response.message || [{ error: 'Could not get client' }];
        }

        return response.response || {};
    }
}

export const ApplicationActionModalContentTag = ApplicationActionModalContent.register();
