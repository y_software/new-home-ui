import { SmartHTMLElement } from './SmartHTMLElement.js';

export class SmartText extends SmartHTMLElement {
    getStyles() {
        return `
            .smart-text,
            ::slotted(*) {
                color: var(--colors--text--normal);
            }
        `;
    }

    getContentElementTag() {
        return 'slot';
    }
}

export const SmartTextTag = SmartText.register();
