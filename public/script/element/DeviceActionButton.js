import { ApiGateway } from '../service/ApiGateway.js';
import { SmartButton } from './SmartButton.js';
import { loadApplicationMessages } from '../functions/deviceActions/loadApplicationMessages.js';
import { loadApplicationModal } from '../functions/deviceActions/loadApplicationModal.js';
import { loadApplicationScripts } from '../functions/deviceActions/loadApplicationScripts.js';
import { loadApplicationView } from '../functions/deviceActions/loadApplicationView.js';

const RESPONSE_HANDLER = [
    loadApplicationScripts,
    loadApplicationView,
    loadApplicationModal,
    loadApplicationMessages,
];

export class DeviceActionButton extends SmartButton {
    apiGateway = new ApiGateway();

    application = '';

    channel = '';

    deviceAction = 'device_action';

    constructor() {
        super();

        this.addEventListener('click', this._onClick.bind(this));
    }

    /**
     * @returns {Promise<void>}
     *
     * @private
     */
    async _onClick() {
        try {
            const response = await this._callDeviceAction();

            for (const handler of RESPONSE_HANDLER) {
                await handler.call(this, response, this.application);
            }
        } catch (e) {
            this.dispatchCustomEvent('smartMessage', { messages: e });
        }
    }

    /**
     * @returns {Promise<object>}
     *
     * @private
     */
    async _callDeviceAction() {
        const response = await this.apiGateway.callMethod(this.application, this.deviceAction, {
            channel: this.channel,
        });

        if (!response.success && !response.response) {
            throw response.message || [{ error: 'Could not get client' }];
        }

        return response.response || {};
    }
}

const Tag = DeviceActionButton.register();

/**
 * @param {string} application
 * @param {string} channel
 * @param {string} [deviceAction]
 * @returns {function(...[*]): HTMLElement}
 * @constructor
 */
export function DeviceActionButtonTag(application, channel, deviceAction) {
    /**
     * @returns {DeviceActionButton}
     */
    return function DeviceActionButtonTagInner(...content) {
        const button = Tag(...content);
        button.deviceAction = deviceAction || 'device_action';
        button.application = application;
        button.channel = channel;

        return button;
    };
}
