import { SmartHTMLElement } from './SmartHTMLElement.js';

class SmartLabel extends SmartHTMLElement {
    getContentElementTag() {
        return 'label';
    }

    getStyles() {
        return `
        .${this.tagName.toLowerCase()} {
            display: block;
            margin-bottom: 0.625em;
        }
        
        ::slotted(input) {
            ${this.getCssVariableFromAttribute('border-color', 'var(--colors--background--floating)')}

            border-radius: 2px;
            border: 0 none;
            border-bottom: var(--border-color) solid 2px;
            font-size: 1em;
            outline: none !important;
            padding: 0.3125em;
            width: 100%;
        }
        `;
    }

    getContent() {
        return document.createElement('slot');
    }
}

export const SmartLabelTag = SmartLabel.register();
