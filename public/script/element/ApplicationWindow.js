import {ApiGateway} from '../service/ApiGateway.js';
import {GridContainer} from './GridContainer.js';
import {SmartHTMLElement} from './SmartHTMLElement.js';

const HASH_START = 1;
const OUT_OF_BOUNDS = -1;
const CSS_ANIMATION_DURATION = 100;

export class ApplicationWindow extends SmartHTMLElement {
    root = document.getElementById('root');

    application = '';

    apiGateway = new ApiGateway();

    hash = '';

    trustedHashes = [];

    constructor() {
        super();

        this.addEventListener('smartMessage', this._onSmartMessage.bind(this));
        this.addEventListener('loadNewRoot', this._onLoadNewRoot.bind(this));
    }

    connectedCallback() {
        super.connectedCallback();

        const windowHash = decodeURI(window.location.hash.substring(HASH_START));

        if (this.hash && windowHash !== this.hash && this.trustedHashes.indexOf(windowHash) === OUT_OF_BOUNDS) {
            window.history.pushState({}, window.location.title, `#${this.hash}`);
        }
    }

    remove() {
        this.classList.add('hide');
        this.style.setProperty('--remove--from-height', `${this.scrollHeight}px`);

        setTimeout(super.remove.bind(this), CSS_ANIMATION_DURATION);
    }

    /**
     * Returns the content of the window
     *
     * @returns {Promise<HTMLElement[]>}
     */
    async getWindowComponents() {
        return [];
    }

    /**
     * Returns the element (tag) that is used for the window components the be contained in.
     * Default is the GridContainer
     *
     * @returns {string}
     */
    getContentElementTag() {
        return GridContainer.tagName;
    }

    /**
     * Used by the SmartHTMLElement to get this elements content
     *
     * @returns {Promise<(HTMLDivElement|HTMLElement)[]>}
     */
    async getContent() {
        this.classList.add('application-window');

        return [...(await this.getWindowComponents())];
    }

    /**
     * Displays an error message above the window content.
     * (only works if `getContent` method is not overridden)
     *
     * @param textContent
     */
    showError(textContent) {
        this.addMessages([
            {
                error: textContent,
            },
        ]);
    }

    /**
     * Displays messages above the window content.
     * (only works if `getContent` method is not overridden)
     *
     * @param messageObjects
     */
    addMessages(messageObjects) {
        if (!Array.isArray(messageObjects)) {
            messageObjects = [messageObjects];
        }

        document.getElementById('messages').dispatchEvent(new CustomEvent(
            'smartMessage',
            { detail: { messages: messageObjects } },
        ));
    }

    /**
     * Calls the given method on the client, this window is meant for.
     * Returns the raw response from the method or an error message if an error occurred
     *
     * @param {string} methodName
     * @param {any} [body] - defaults to null
     *
     * @returns {Promise<{code: number, message: (*|{error: string}[])}>}
     */
    async callOwnMethod(methodName, body) {
        const response = await this.apiGateway.callMethod(this.application, methodName, body);

        if (!response.success) {
            return {
                code: 1,
                message: response.message || [{ error: 'Could not call method' }],
            };
        }

        return response.response || {};
    }

    /**
     * @param {{messages: Array}} detail
     *
     * @private
     */
    _onSmartMessage({ detail }) {
        this.addMessages(detail.messages);
    }

    /**
     * @param {{component: HTMLElement}} detail
     *
     * @private
     */
    _onLoadNewRoot({ detail }) {
        this.root.childNodes.forEach((node) => node.remove());
        this.root.appendChild(detail.component);
    }
}
