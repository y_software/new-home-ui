import { Div } from '../functions/Html.js';
import { Language } from '../service/LanguageLoader.js';
import { SmartButtonTag } from './SmartButton.js';
import { SmartHTMLElement } from './SmartHTMLElement.js';
import { SmartTitleTag } from './SmartTitle.js';

const CSS_ANIMATION_DURATION = 200;

export class SmartModal extends SmartHTMLElement {
    askClose = false;

    submitButton = SmartButtonTag`Submit`;

    closeButton = SmartButtonTag`Close`;

    modalTitle = SmartTitleTag.center`Modal`;

    constructor() {
        super();

        this.submitButton.setAttribute('thin', '');
        this.submitButton.setAttribute('small', '');
        this.submitButton.addEventListener('click', this._onSubmitButtonClick.bind(this));

        this.closeButton.setAttribute('thin', '');
        this.closeButton.setAttribute('small', '');
        this.closeButton.setAttribute('type', 'error');
        this.closeButton.addEventListener('click', this.close.bind(this));
    }

    set submitText(innerText) {
        this.submitButton.innerText = innerText;
    }

    set closeText(innerText) {
        this.closeButton.innerText = innerText;
    }

    set title(title) {
        this.modalTitle.innerText = title;
    }

    getContentElementTag() {
        return 'div';
    }

    /* eslint-disable */

    getStyles() {
        return `
        .${this.tagName.toLowerCase()} {
            animation: appear-from-top ${CSS_ANIMATION_DURATION}ms;
            backdrop-filter: blur(2px);
            background: rgba(0, 0, 0, 0.3);
            height: 100vh;
            left: 0;
            position: fixed;
            top: 0;
            width: 100vw;
        }

        .${this.tagName.toLowerCase()}.hide {
            animation: disappear-to-top ${CSS_ANIMATION_DURATION}ms reverse;
            opacity: 0;
            scale: 1.5;
        }

        .modal {
            ${this.getCssVariableFromAttribute('background-color', 'var(--colors--background--static)')}
        
            background-color: var(--background-color);
            box-shadow: 0 0 0.5em rgba(0, 0, 0, 0.25);
            box-sizing: border-box;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -40%);
            top: 40%;
            width: 40%;
            min-width: 320px;
            padding: 0.625em 1em;
            max-height: 90vh;
            overflow: auto;
        }
        
        .modal--headline {
            margin-bottom: 0.625em;
        }
        
        .modal--content {
            margin-bottom: 0.625em;
        }

        .modal--headline ::slotted(*) {
            margin: 0;
        }
        
        .modal--actions {
            display: flex;
        }
        
        .modal--actions :first-child {
            margin-right: auto;
        }
        
        @keyframes appear-from-top {
            0% {
                opacity: 0;
                scale: 1.5;
            }

            5% {
                scale: 1.5;
            }

            100% {
                opacity: 1;
                scale: 1;
            }
        }
        
        @keyframes disappear-to-top {
            0% {
                opacity: 0;
                scale: 1.5;
            }

            5% {
                scale: 1.5;
            }

            100% {
                opacity: 1;
                scale: 1;
            }
        }
        `;
    }

    /* eslint-enable */

    async getContent() {
        this.submitButton.innerText = await Language.modal_button_submit;
        this.closeButton.innerText = await Language.modal_button_close;

        const content = Div`
            <div class="modal--headline">${this.modalTitle}</div>
            <div class="modal--content"><slot/></div>
            <div class="modal--actions">${this.submitButton}<slot name="actions"></slot>${this.closeButton}</div>
        `;

        content.classList.add('modal');
        content.addEventListener('mousedown', (ev) => ev.stopPropagation());

        return content;
    }

    remove() {
        this.content.classList.add('hide');

        setTimeout(super.remove.bind(this), CSS_ANIMATION_DURATION);
    }

    close() {
        if ((this.askClose && confirm('Do you really want to close the modal?')) || !this.askClose) {
            this.remove();
        }
    }

    _addChildNodes(contentNode, elementContent) {
        contentNode.addEventListener('mousedown', this.close.bind(this));

        return super._addChildNodes(contentNode, elementContent);
    }

    _onSubmitButtonClick() {
        this.dispatchCustomEvent('SmartModal.submit', { closeModal: this.remove.bind(this) });
    }
}

export const SmartModalTag = SmartModal.register();
