/**
 * @this {SmartHTMLElement&{application: string, channel: string}}
 *
 * @param {object} response
 * @param {object} response.message
 * @param {string} response.message.component
 * @return {Promise<void>}
 */
export async function loadApplicationView(response) {
    if (response.message && response.message.component) {
        const applicationComponent = document.createElement(response.message.component);
        applicationComponent.application = this.application;
        applicationComponent.channel = this.channel;
        this.dispatchCustomEvent('loadNewRoot', { component: applicationComponent });
    }
}
