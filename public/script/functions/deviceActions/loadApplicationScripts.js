import { ScriptLoader } from '../../service/ScriptLoader.js';

const scriptLoader = new ScriptLoader();

/**
 * @this {SmartHTMLElement&{application: string, channel: string}}
 *
 * @param {object} response
 * @param {?string} application
 * @param {object} response.message
 * @param {string[]} response.message.scripts
 * @return {Promise<void>}
 */
export async function loadApplicationScripts(response, application = null) {
    if (response.message && response.message.scripts) {
        try {
            await scriptLoader.loadScripts(response.message.scripts, { clientName: application });
        } catch (e) {
            throw new Error('Could not load scripts');
        }
    }
}
