const SUPPORTED_TYPES = [
    'error',
    'message',
    'markdown',
    'success',
    'debug',
    'info',
];

/**
 * @this {SmartHTMLElement&{application: string, channel: string}}
 *
 * @param {object} message
 * @return {void}
 */
function loadApplicationMessage(message) {
    const filteredMessages = {};

    for (const type of SUPPORTED_TYPES) {
        if (message[type]) {
            filteredMessages[type] = message[type];
        }
    }

    this.dispatchCustomEvent('smartMessage', { messages: filteredMessages });
}

/**
 * @this {DeviceActionButton}
 *
 * @param {object} response
 * @param {object|object[]} response.message
 * @return {Promise<void>}
 */
export async function loadApplicationMessages(response) {
    if (Array.isArray(response.message)) {
        for (const message of response.message) {
            loadApplicationMessage.call(this, message);
        }

        return;
    }

    loadApplicationMessage.call(this, response.message);
}
