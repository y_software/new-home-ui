import { SmartModalTag } from '../../element/SmartModal.js';

/**
 * @param {object} modalContext
 * @param {string} [modalContext.close]
 * @param {string} [modalContext.submit]
 * @param {string} [modalContext.title]
 * @param {SmartModal} modal
 */
function setModalFields(modalContext, modal) {
    if (modalContext.title) {
        modal.title = modalContext.title;
    }

    if (modalContext.submit) {
        modal.submitText = modalContext.submit;
    }

    if (modalContext.close) {
        modal.closeText = modalContext.close;
    }
}

/**
 * @this {SmartHTMLElement&{application: string, channel: string}}
 *
 * @param {string} modalContext.component
 * @param {string} [modalContext.close]
 * @param {string} [modalContext.submit]
 * @param {string} [modalContext.title]
 * @returns {HTMLElement}
 */
function buildModalComponent(modalContext) {
    const modalComponent = document.createElement(modalContext.component);
    modalComponent.application = this.application;
    modalComponent.channel = this.channel;
    modalComponent.window = this;

    const modal = SmartModalTag`${modalComponent}`;

    setModalFields(modalContext, modal);

    return modal;
}

/**
 * @this {DeviceActionButton}
 *
 * @param {object} response
 * @param {object} response.message
 * @param {object} response.message.modal
 * @param {string} response.message.modal.component
 * @param {string} response.message.modal.submit
 * @param {string} response.message.modal.close
 * @return {Promise<void>}
 */
export async function loadApplicationModal(response) {
    if (response.message && response.message.modal) {
        document.body.appendChild(buildModalComponent.call(this, response.message.modal));
    }
}
