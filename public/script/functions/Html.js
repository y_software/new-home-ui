const RANDOM_LENGTH_MULTIPLY = 1000;
const FIRST_ITEM = 0;

/**
 * The callback for the modify substitution option.
 * Modifies a given element from the tag functions substitution and returns the modified version
 *
 * @callback modifySubstitutionCallback
 *
 * @param {HTMLElement} element
 *
 * @returns {HTMLElement}
 */

/**
 * The callback for the optionHandler
 *
 * @callback optionsHandlerCallback
 *
 * @param {SmartHTMLElement} element The element on which the option should be set
 * @param {any} value The value that should be set
 * @param {string} name The name of the property that is currently handled
 */

/**
 *The callback returned from the supportsOptions function
 *
 * @callback supportsOptionsTag
 *
 * @param {object} options The options accepted by the element
 *
 * @return {tagFunctionCallback}
 */

/**
 * This class builds tag functions for html elements.
 * The resulting tag function can be retrieved with the `gatTagFunction` method.
 * The tag function will automatically return a joined string with properly inserted {HTMLElement}s
 * if used in substitution.
 *
 * @class
 */
export class Html {
    /**
     * Adds a nested function to the given tag function which can be called with the modifier.
     * When called the modifier will be added as attribute to the element.
     *
     * @example ```HTML
     * <script>
     *   addModifier(Div, 'center');
     *   Div.center`Content`;
     * </script>
     * // Results in
     * <div center>Content</div>
     *  ```
     *
     * @param {string[]|string} attributes - defaults to the modifier
     *
     * @return {tagFunctionCallback}
     */
    static addModifier(attributes) {
        if (!Array.isArray(attributes)) {
            attributes = [attributes];
        }

        /**
         * @this {tagFunctionCallback}
         */
        return function tagFunction(...content) {
            const element = this(...content);

            for (const attribute of attributes) {
                element.setAttribute(attribute, '');
            }

            return element;
        };
    }

    /**
     * @param {tagFunctionCallback} tagFunction
     * @param {Object.<string, optionsHandlerCallback>} optionHandler
     *
     * @return {supportsOptionsTag}
     */
    static supportsOptions(optionHandler) {
        /**
         * @this {tagFunctionCallback}
         */
        return function tagFunctionOptions(options) {
            /**
             * @this {tagFunctionCallback}
             */
            return (function tagFunctionOptionsInner(...content) {
                const element = this(...content);

                for (const [name, value] of Object.entries(options)) {
                    if (Object.prototype.hasOwnProperty.call(optionHandler, name)) {
                        optionHandler[name](element, value, name);
                    }
                }

                return element;
            }).bind(this);
        };
    }

    /**
     * @param {string} elementTag
     * @param {{modifyCallback: modifySubstitutionCallback | null}} options
     */
    constructor(elementTag, options = {}) {
        this.elementTag = elementTag;
        this.options = options;

        if (typeof this.options.modifyCallback !== 'function') {
            this.options.modifyCallback = (v) => v;
        }
    }

    /**
     * This function will return a tag function with the, in the constructor given, element tag and options
     *
     * @returns {tagFunctionCallback}
     */
    getTagFunction() {
        const tagFunction = this._tagFunction.bind(this);

        tagFunction.options = Html.supportsOptions(Html.defaultOptions);

        return tagFunction;
    }

    /**
     * The function resulting from the `getTagFunction` call.
     * The tag function will join template strings and properly insert html elements if used in the template as
     * variables.
     *
     * @callback tagFunctionCallback
     * @template T
     *
     * @param {string[]} raw
     * @param {*} substitutions
     *
     * @returns {T}
     *
     * @private
     */
    _tagFunction(raw, ...substitutions) {
        if (!this) {
            throw new Error('You have to implement the html tag function first!');
        }

        const substId = Math.round(Math.random() * RANDOM_LENGTH_MULTIPLY);
        const counter = { id: 0 };
        const elements = {};

        substitutions = substitutions.map(this._filterSubstitutions.bind(this, {
            counter,
            elements,
            substId,
        }));

        const root = document.createElement(this.elementTag ?? 'div');
        root.innerHTML = String.raw(raw, ...substitutions);

        this._translateSubsts(root, elements);

        return root;
    }

    /**
     * This function is used as the callback for the array mapping of the template substitutions.
     * It will store variables of the {HTMLElement} type in the elements object and return a placeholder element
     * with an id associated with the original {HTMLElement}.
     *
     * @param {Object} substInfo
     * @param {number} substInfo.substId
     * @param {{id: number}} substInfo.counter
     * @param {Object.<string, HTMLElement>} substInfo.elements
     * @param {Node} element
     *
     * @returns {string}
     *
     * @private
     */
    _filterSubstitutions(substInfo, element) {
        if (Array.isArray(element)) {
            let out = '';

            for (const node of element) {
                out += this._prepareElement(substInfo, node);
            }

            return out;
        }

        return this._prepareElement(substInfo, element);
    }

    /**
     * @param {Object} substInfo
     * @param {number} substInfo.substId
     * @param {{id: number}} substInfo.counter
     * @param {Object.<string, HTMLElement>} substInfo.elements
     * @param element
     *
     * @returns {string|*}
     *
     *  @private
     */
    _prepareElement(substInfo, element) {
        if (!(element instanceof HTMLElement)) {
            return element;
        }

        const elementSubstId = `subst-${substInfo.substId}-${substInfo.counter.id}`;

        substInfo.elements[elementSubstId] = this.options.modifyCallback(element);
        substInfo.counter.id++;

        const div = document.createElement('div');
        div.classList.add(elementSubstId);

        return div.outerHTML;
    }

    /**
     * This method replaces the placeholder elements from the filter with the original elements from the substitution
     *
     * @param root
     * @param elements
     *
     * @private
     */
    _translateSubsts(root, elements) {
        for (const [id, element] of Object.entries(elements)) {
            root.getElementsByClassName(id).item(FIRST_ITEM).replaceWith(element);
        }
    }
}

Html.defaultOptions = {
    /**
     * @param {SmartTitle} element
     * @param {[string]} value
     */
    classList(element, value) {
        element.classList.add(...value);
    },
};

/**
 * @type {tagFunctionCallback<HTMLDivElement>}
 */
export const Div = new Html('div').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLSpanElement>}
 */
export const Span = new Html('span').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLParagraphElement>}
 */
export const P = new Html('p').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLLabelElement>}
 */
export const Label = new Html('label').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLLIElement>}
 */
export const Li = new Html('li').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLUListElement>}
 */
export const Ul = new Html('ul').getTagFunction();

/**
 * @type {tagFunctionCallback<HTMLFormElement>}
 */
export const Form = new Html('form').getTagFunction();
