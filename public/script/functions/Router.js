import {DashboardView} from '../views/DashboardView.js';

const HASH_START = 1;

export class Router {
    root = document.getElementById('root');

    routes = {};

    constructor(routes) {
        this.routes = routes;
    }

    /**
     * @param {Function & {tagName: string}} component
     * @param {string} path
     */
    registerRoute(component, path) {
        this.routes[path] = component;
    }

    registerEvents() {
        window.addEventListener('hashchange', this._handleHashChange.bind(this));
        this._handleHashChange();
    }

    /**
     * @private
     */
    _handleHashChange() {
        const hashUrl = decodeURI(window.location.hash.substring(HASH_START));

        for (const [route, component] of Object.entries(this.routes)) {
            const regex = new RegExp(route);
            const result = regex.exec(hashUrl);

            if (!result) {
                continue;
            }

            this._loadHashView(component, result.groups || {});

            return;
        }

        this._loadHashView(DashboardView, {});
    }

    /**
     * @param {Object.<string, number>} template
     * @param {[string]} result
     *
     * @returns {Object.<string, string>}
     *
     * @private
     */
    _buildArgs(template, result) {
        const args = {};

        for (const [key, index] of Object.entries(template)) {
            args[key] = result[index] || null;
        }

        return args;
    }

    /**
     * @param {Function & {tagName: string}} component
     * @param {Object.<string, string>} args
     *
     * @private
     */
    _loadHashView(component, args) {
        const newRoot = document.createElement(component.tagName);

        for (const [key, value] of Object.entries(args)) {
            newRoot[key] = value;
        }

        this.root.childNodes.forEach((node) => node.remove());
        this.root.appendChild(newRoot);
    }
}

export const DefaultRouter = new Router({});
