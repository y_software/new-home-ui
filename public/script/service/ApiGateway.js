import { DefaultSettingsManager } from './setup/SettingsManager.js';

const API_CALL_METHOD = 'CALL';
const API_REMOVE_METHOD = 'DELETE';
const API_CREATE_METHOD = 'PUT';

/**
 * Generates an URL with the API constants
 *
 * @param {string} text
 * @param {object} values
 * @returns {string}
 */
export function apiUrl(text, ...values) {
    const mainUrl = String.raw(text, ...values);
    const type = DefaultSettingsManager.getSetting('connection_type');

    if (type === 'Core') {
        return `${DefaultSettingsManager.getSetting('api_prepend')}${mainUrl}${
            DefaultSettingsManager.getSetting('api_append')
        }`;
    }

    if (type === 'Proxy') {
        const url = `${DefaultSettingsManager.getSetting('proxy_url')}proxy/${
            DefaultSettingsManager.getSetting('client_id')
        }/${mainUrl}`;

        return url;
    }

    return mainUrl;
}

export class ApiGateway {
    /**
     * Gets all clients
     *
     * @returns {Promise<{success: boolean, list: string[]}>}
     */
    async getClients() {
        return this.get(apiUrl`client`);
    }

    /**
     * Gets all rooms
     *
     * @returns {Promise<{success: boolean, list: string[]}>}
     */
    async getRooms() {
        return this.get(apiUrl`room`);
    }

    /**
     * Gets all devices in a room
     *
     * @param {string} room
     * @returns {Promise<{success: boolean, table: {[string]: {application: string, channel: string}}}>}
     */
    async getDevices(room) {
        return this.get(apiUrl`room/${room}/device`);
    }

    /**
     * @param {string} clientName
     * @param {string} methodName
     * @param {object} [body] - defaults to null
     * @returns {Promise<Response>}
     */
    async callMethod(clientName, methodName, body) {
        const url = apiUrl`client/${clientName}/method/${methodName}`;
        const request = this._createRequest(url, API_CALL_METHOD, body);

        return fetch(request).then(this._refineFetch.bind(this));
    }

    /**
     * Executes a PUT call to the given URL with the given body
     * @param {string} url
     * @param {object} [body]
     * @returns {Promise<Response>}
     */
    async get(url, body) {
        const request = this._createRequest(url, 'GET', body);

        return fetch(request).then(this._refineFetch.bind(this));
    }

    /**
     * Executes a PUT call to the given URL with the given body
     * @param {string} url
     * @param {object} [body]
     * @returns {Promise<Response>}
     */
    async create(url, body) {
        const request = this._createRequest(url, API_CREATE_METHOD, body);

        return fetch(request).then(this._refineFetch.bind(this));
    }

    /**
     * Executes a PUT call to the given URL with the given body
     * @param {string} url
     * @returns {Promise<Response>}
     */
    async remove(url) {
        const request = this._createRequest(url, API_REMOVE_METHOD, {});

        return fetch(request).then(this._refineFetch.bind(this));
    }

    /**
     * @param {string} url
     * @param {string} method
     * @param {object} [body]
     * @returns {Request}
     * @private
     */
    _createRequest(url, method, body) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (DefaultSettingsManager.getSetting('connection_type')) {
            headers.append('Authorization', `Basic ${  btoa(`${DefaultSettingsManager.getSetting('proxy_username')}:${
                DefaultSettingsManager.getSetting('proxy_password')
            }`)}`);
        }

        return new Request(url, {
            body: JSON.stringify(body),
            headers,
            method,
            mode: 'cors',
            url,
        });
    }

    /**
     * Checks if the request was successful, if not appends an error message
     *
     * @param {any} response
     * @returns {{success: boolean, message: [{error: string}]}|any}
     *
     * @private
     */
    _refineFetch(response) {
        try {
            return response.json();
        } catch {
            return {
                message: [
                    {
                        error: `Could not complete response: ${response.statusText}`,
                    },
                ],
                success: false,
            };
        }
    }
}
