const LANGUAGES = ['en', 'de'];

const OUT_OF_BOUNDS = -1;

export class LanguageLoader {
    static loadedLanguage = false;

    constructor() {
        // eslint-disable-next-line no-constructor-return
        return new Proxy(this, { get: this._get.bind(this) });
    }

    /**
     * @param {LanguageLoader} self
     * @param {string} name
     *
     * @return {Promise<string>}
     *
     * @private
     */
    async _get(self, name) {
        if (!LanguageLoader.loadedLanguage) {
            await this._loadLanguage();
        }

        return LanguageLoader.loadedLanguage[name] || name;
    }

    /**
     * @return {Promise<void>}
     * @private
     */
    async _loadLanguage() {
        for (const language of navigator.languages) {
            if (LANGUAGES.indexOf(language) > OUT_OF_BOUNDS) {
                LanguageLoader.loadedLanguage = await (await fetch(`/lang/${language}.json`)).json();

                return;
            }
        }
    }
}

/**
 * @type {Object<String, Promise<String>>}
 */
export const Language = new LanguageLoader();
