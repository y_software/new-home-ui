import { Debug } from './messageTypes/Debug.js';
import { Error } from './messageTypes/Error.js';
import { Info } from './messageTypes/Info.js';
import { List } from './messageTypes/List.js';
import { Markdown } from './messageTypes/Markdown.js';
import { Message } from './messageTypes/Message.js';
import { Success } from './messageTypes/Success.js';
import { Table } from './messageTypes/Table.js';

export class MessageBuilder {
    constructor() {
        this.types = new Map();

        this.types.set('error', Error);
        this.types.set('info', Info);
        this.types.set('debug', Debug);
        this.types.set('success', Success);
        this.types.set('markdown', Markdown);
        this.types.set('message', Message);
        this.types.set('list', List);
        this.types.set('table', Table);
    }

    /**
     * @param {{[string]: string}} message
     * @returns {HTMLElement[]}
     */
    buildMessage(message) {
        return [].concat(this._buildMessageType(message));
    }

    /**
     * @param {{[string]: string}} message
     * @returns {HTMLElement[]}
     *
     * @private
     */
    _buildMessageType(message) {
        const messages = [];

        for (const [type, content] of Object.entries(message)) {
            const handlerConstructor = this._getMessageTypeHandler(type);
            messages.push(new handlerConstructor(content).build());
        }

        return messages;
    }

    getStyle() {
        let style = '';

        for (const [, value] of this.types) {
            style += value.style;
        }

        return style;
    }

    /**
     * @param {{[string]: string}} type
     * @returns {Function}
     * @private
     */
    _getMessageTypeHandler(type) {
        const typeHandler = this.types.get(type);

        if (typeHandler) {
            return typeHandler;
        }

        return Error;
    }
}
