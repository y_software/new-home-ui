import { MessageType } from '../MessageType.js';

export class Table extends MessageType {
    static get style() {
        return '';
    }

    constructor(table) {
        super();

        this.table = table;
    }

    build() {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const columns = this._getTableColumns();

        this._buildHeaderRow(thead, columns);
        table.appendChild(thead);
        table.appendChild(this._buildContentRows(columns));

        return table;
    }

    _buildHeaderRow(thead, columns) {
        const tr = document.createElement('tr');

        for (const column of columns) {
            const th = document.createElement('th');
            th.innerHTML = column;

            tr.appendChild(th);
        }

        thead.appendChild(tr);
    }

    _getTableColumns() {
        const columns = [];

        for (const row of this.table) {
            for (const [name] of Object.entries(row)) {
                columns.push(name);
            }
        }

        return columns.filter((value, index, array) => array.indexOf(value) === index);
    }

    _buildContentRows(columns) {
        const tbody = document.createElement('tbody');

        for (const row of this.table) {
            tbody.appendChild(this._buildContentRow(row, columns));
        }

        return tbody;
    }

    _buildContentRow(row, columns) {
        const tableRow = document.createElement('tr');

        for (const column of columns) {
            tableRow.appendChild(this._buildContentCell(row[column] || ''));
        }

        return tableRow;
    }

    _buildContentCell(value) {
        const tableCell = document.createElement('td');

        tableCell.innerHTML = value;

        return tableCell;
    }
}
