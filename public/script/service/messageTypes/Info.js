import { Message } from './Message.js';

export class Info extends Message {
    static get style() {
        return `
        .message--info {
            --accent-color: var(--colors--message--info);
            --text-color: #fff;
        }
        `;
    }

    getAdditionalClasses() {
        return ['message--info'];
    }
}
