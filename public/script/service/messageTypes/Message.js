import { MessageType } from '../MessageType.js';
import { P } from '../../functions/Html.js';

export class Message extends MessageType {
    static get style() {
        return `
        .message--message {
            --accent-color: #eee;
            --text-color: #000;
        }

        .smart-message-child {
            box-sizing: border-box;
            color: var(--colors--text--normal);
            margin: 0;
            padding: 0.5em 1em;
        }
        `;
    }

    static createMessageObject(message) {
        return { [this.name.toLowerCase()]: message };
    }

    constructor(message) {
        super();

        this.message = message;
    }

    getAdditionalClasses() {
        return ['message--message'];
    }

    build() {
        const element = P`${this.message}`;
        element.classList.add('smart-message-child');
        element.classList.add(...this.getAdditionalClasses());

        return element;
    }
}
