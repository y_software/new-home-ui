import { Message } from './Message.js';

export class Debug extends Message {
    static get style() {
        return `
        .message--debug {
            --accent-color: var(--colors--message--debug);
            --text-color: #fff;
        }
        `;
    }

    getAdditionalClasses() {
        return ['message--debug'];
    }
}
