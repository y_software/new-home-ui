import { Message } from './Message.js';

export class Markdown extends Message {
    static get style() {
        return `
        .message--markdown {
            --accent-color: #eee;
            --text-color: #000;
        }`;
    }

    getAdditionalClasses() {
        return ['message--markdown'];
    }
}
