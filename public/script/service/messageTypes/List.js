import { MessageType } from '../MessageType.js';

export class List extends MessageType {
    static get style() {
        return '';
    }

    constructor(items) {
        super();

        this.items = items;
    }

    build() {
        const list = document.createElement('ul');

        list.append(...this.items.map(this._buildListItem.bind(this)));

        return list;
    }

    _buildListItem(itemText) {
        const item = document.createElement('li');
        item.innerHTML = itemText;

        return item;
    }
}
