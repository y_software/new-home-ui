import { Message } from './Message.js';

export class Error extends Message {
    static get style() {
        return `
        .message--error {
            --accent-color: var(--colors--message--error);
            --text-color: #fff;
        }
        `;
    }

    getAdditionalClasses() {
        return ['message--error'];
    }
}
