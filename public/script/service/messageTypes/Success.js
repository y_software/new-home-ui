import { Message } from './Message.js';

export class Success extends Message {
    static get style() {
        return `
        .message--success {
            --accent-color: var(--colors--message--success);
            --text-color: #fff;
        }
        `;
    }

    getAdditionalClasses() {
        return ['message--success'];
    }
}
