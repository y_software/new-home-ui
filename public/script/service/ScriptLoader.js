import { apiUrl } from './ApiGateway.js';

const INDEX_NOT_FOUND = -1;
const URL_APP_PREFIX_LENGTH = 4;
const RESOLVABLE_SYMBOLS = {
    /**
     * @param {string} url
     * @param {object} parameters
     * @param {string} parameters.clientName
     * @returns {string}
     */
    '@app': function appHandle(url, parameters) {
        const baseUrl = apiUrl`client/${parameters.clientName}/script`;

        url = `@app${url.substr(URL_APP_PREFIX_LENGTH)}`;

        return url.replace('@app', baseUrl);
    },
};

export class ScriptLoader {
    /**
     * Same as loadScript, but with multiple scripts
     *
     * @param {string[]} scriptUrls
     * @param {object} symbolParameter
     * @param {?string} symbolParameter.clientName
     * @returns {Promise<void[]>}
     */
    async loadScripts(scriptUrls, symbolParameter = {}) {
        return Promise.all(scriptUrls.map((scriptUrl) => this.loadScript(scriptUrl, symbolParameter)));
    }

    /**
     * Creates an html script tag, appends it to the DOM and waits for it to finish load
     *
     * @param {string} scriptUrl
     * @param {object} symbolParameter
     * @param {?string} symbolParameter.clientName
     * @returns {Promise<void>}
     */
    loadScript(scriptUrl, symbolParameter = {}) {
        scriptUrl = this._handleScriptUrlSymbols(scriptUrl, symbolParameter);

        return import(scriptUrl);
    }

    /**
     * @param url
     * @param {object} symbolParameter
     * @param {?string} symbolParameter.clientName
     * @returns {string}
     *
     * @private
     */
    _handleScriptUrlSymbols(url, symbolParameter) {
        for (const [symbol, converter] of Object.entries(RESOLVABLE_SYMBOLS)) {
            if (url.indexOf(symbol) === INDEX_NOT_FOUND) {
                continue;
            }

            url = converter(url, symbolParameter);
        }

        return url;
    }
}
