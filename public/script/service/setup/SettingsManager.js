const ITEM_SETTINGS = 'settings';

export class SettingsManager {
    settings = {};

    constructor() {
        this.load();
    }

    save(settings) {
        localStorage.setItem(ITEM_SETTINGS, JSON.stringify(settings));
        localStorage.setItem(SettingsManager.ITEM_SETUP_COMPLETE, SettingsManager.ITEM_SETUP_COMPLETE_VALUE);

        // This console has some serious information, so it stays!
        // eslint-disable-next-line no-console
        this._sendStorageToWorker().catch(console.error);

        this.load();
    }

    load() {
        this.settings = JSON.parse(localStorage.getItem(ITEM_SETTINGS)) || {};
    }

    /**
     * @param {string} name
     */
    getSetting(name) {
        return this.settings[name];
    }

    async _sendStorageToWorker() {
        const worker = await this._getWorker();

        if (worker) {
            worker.postMessage({
                settings: JSON.parse(localStorage.getItem(ITEM_SETTINGS)),
            });
        }
    }

    async _getWorker() {
        if (!navigator.serviceWorker) {
            return null;
        }

        if (navigator.serviceWorker.controller) {
            return navigator.serviceWorker.controller;
        }

        return (await navigator.serviceWorker.ready).active;
    }
}

export const DefaultSettingsManager = new SettingsManager();

SettingsManager.ITEM_SETUP_COMPLETE = 'setup_complete';
SettingsManager.ITEM_SETUP_COMPLETE_VALUE = true;
