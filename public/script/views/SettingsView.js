import { ApplicationWindow } from '../element/ApplicationWindow.js';
import { DashboardView } from './DashboardView.js';
import { DefaultSettingsManager } from '../service/setup/SettingsManager.js';
import { Div } from '../functions/Html.js';
import { Language } from '../service/LanguageLoader.js';
import { PasswordInputTag } from '../element/form/PasswordInput.js';
import { SelectInputTag } from '../element/form/SelectInput.js';
import { SmartButtonTag } from '../element/SmartButton.js';
import { SmartFormTag } from '../element/form/SmartForm.js';
import { SmartTitleTag } from '../element/SmartTitle.js';
import { TextInputTag } from '../element/form/TextInput.js';

export class SettingsView extends ApplicationWindow {
    hash = 'settings';

    connectionType = SelectInputTag(
        'connection_type',
        [
            'Core',
            'Proxy',
        ],
    )`Server Type`;

    apiPrepend = TextInputTag('api_prepend')`API URL`;

    apiAppend = TextInputTag('api_append')`API Suffix`;

    proxyUrl = TextInputTag('proxy_url')`Proxy URL`;

    clientId = TextInputTag('client_id')`Client Name`;

    proxyUsername = TextInputTag('proxy_username')`Proxy Username`;

    proxyPassword = PasswordInputTag('proxy_password')`Proxy Password`;

    /**
     * @type {SmartForm}
     */
    form = SmartFormTag.options({
        inputs: [],
    })``;

    manager = DefaultSettingsManager;

    saveButton = SmartButtonTag.small`Save`;

    constructor() {
        super();

        this.saveButton.addEventListener('click', this._onClickSave.bind(this));
        this._setupConnectionFields(this.form.addInput.bind(this.form));
    }

    connectedCallback() {
        super.connectedCallback();

        for (const input of this.form.inputs) {
            input.value = this.manager.settings[input.name] || '';
        }
    }

    /**
     * @returns {Promise<HTMLElement[]>}
     */
    async getWindowComponents() {
        this.saveButton.innerText = await Language.view_settings_save;

        return [
            SmartTitleTag.options({ classList: ['grid-container--full-width'] })`${await Language.view_settings_title}`,
            this.form,
            Div.options({ classList: ['grid-container--full-width'] })`${this.saveButton}`,
        ];
    }

    /**
     * @param {Function} add
     * @private
     */
    _setupConnectionFields(add) {
        this.connectionType.select.addEventListener('input', this._onConnectionTypeChange.bind(this));
        this.connectionType.select.addEventListener('change', this._onConnectionTypeChange.bind(this));

        add(this.connectionType);
        add(this.apiPrepend);
        add(this.apiAppend);
        add(this.proxyUrl);
        add(this.clientId);
        add(this.proxyUsername);
        add(this.proxyPassword);

        this._onConnectionTypeChange();
    }

    _onConnectionTypeChange() {
        this.apiPrepend.classList.add('only--core', 'only--connection-type');
        this.apiAppend.classList.add('only--core', 'only--connection-type');
        this.proxyUrl.classList.add('only--proxy', 'only--connection-type');
        this.clientId.classList.add('only--proxy', 'only--connection-type');
        this.proxyUsername.classList.add('only--proxy', 'only--connection-type');
        this.proxyPassword.classList.add('only--proxy', 'only--connection-type');

        this.form.inputs.map((input) => {
            if (!input.classList.contains('only--connection-type')) {
                return input;
            }

            input.style.display = 'none';

            if (input.classList.contains(`only--${  this.connectionType.value.toLowerCase()}`)) {
                input.style.display = '';
            }

            return input;
        });
    }

    _onClickSave() {
        const data = this.form.getData();

        this.manager.save(data);
        this.dispatchCustomEvent('loadNewRoot', { component: document.createElement(DashboardView.tagName) });
    }
}

export const SettingsViewTag = SettingsView.register();
