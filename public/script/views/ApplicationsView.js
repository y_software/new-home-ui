import { AddApplicationModalView } from './modal/AddApplicationModalView.js';
import { ApplicationWindow } from '../element/ApplicationWindow.js';
import { DeviceActionButtonTag } from '../element/DeviceActionButton.js';
import { Div } from '../functions/Html.js';
import { Language } from '../service/LanguageLoader.js';
import { SmartModalTag } from '../element/SmartModal.js';
import { SmartTitleTag } from '../element/SmartTitle.js';

const CLIENTS_MIN_LENGTH = 1;

export class ApplicationsView extends ApplicationWindow {
    hash = 'applications';

    getStyles() {
        return `${super.getStyles()}
            .button--add {
                margin-left: 1em;
            }

            .title-line {
                align-items: center;
                display: flex;
                width: 100%;
            }

            .button--add {
                background: none;
                border: none;
                color: var(--colors--message--success);
                font-weight: bold;
                height: 1.2em;
                font-size: 1.5em;
                padding: 0;
                width: 1.2em;
            }
        `;
    }

    /**
     * @returns {Promise<[HTMLElement]>}
     */
    async getWindowComponents() {
        const clients = await this._loadClients();
        const addButton = document.createElement('button');
        addButton.innerHTML = '+';
        addButton.classList.add('button--add');
        addButton.addEventListener('click', this._onClickAddClient.bind(this));

        const titleLine = Div`<smart-title>${await Language.view_applications_title}</smart-title>${addButton}`;
        titleLine.classList.add('title-line');
        titleLine.classList.add('grid-container--full-width');

        return [
            titleLine,
            ...await this._buildClients(clients),
        ];
    }

    /**
     * @returns {Promise<string[]>}
     *
     * @private
     */
    async _loadClients() {
        return (await this.apiGateway.getClients()).list || [];
    }

    /**
     * @param {string[]} clients
     *
     * @returns {HTMLElement[]}
     *
     * @private
     */
    async _buildClients(clients) {
        if (clients.length < CLIENTS_MIN_LENGTH) {
            return [SmartTitleTag.center`${await Language.view_applications_no_applications}`];
        }

        return clients.map(this._buildClient.bind(this));
    }

    /**
     * @param {string} client
     *
     * @returns {DeviceActionButton}
     *
     * @private
     */
    _buildClient(client) {
        return DeviceActionButtonTag(client, 'settings', 'config_root')`${client}`;
    }

    /**
     * @private
     */
    async _onClickAddClient() {
        const modalView = document.createElement(AddApplicationModalView.tagName);
        modalView.window = this;

        const modal = SmartModalTag`${modalView}`;
        modal.title = await Language.modal_add_application_title;
        modal.closeText = await Language.modal_button_cancel;

        document.body.appendChild(modal);
    }
}

ApplicationsView.register();
