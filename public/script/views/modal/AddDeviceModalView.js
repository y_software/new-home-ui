import { ApiGateway, apiUrl } from '../../service/ApiGateway.js';
import { DevicesView } from '../DevicesView.js';
import { SelectInputTag } from '../../element/form/SelectInput.js';
import { SmartFormTag } from '../../element/form/SmartForm.js';
import { SmartHTMLElement } from '../../element/SmartHTMLElement.js';
import { TextInputTag } from '../../element/form/TextInput.js';

export class AddDeviceModalView extends SmartHTMLElement {
    apiGateway = new ApiGateway();

    /**
     * @type {SmartForm}
     */
    form = SmartFormTag``;

    messageContainer = document.getElementById('messages');

    /**
     * @type {string}
     */
    room;

    /**
     * @type {SmartHTMLElement}
     */
    window;

    async getContent() {
        this.parentElement.addEventListener('SmartModal.submit', this._onModalSubmit.bind(this));

        this.form.addInput(TextInputTag('name')`Name`);
        this.form.addInput(await this._createDeviceSelect());
        this.form.addInput(TextInputTag('channel')`Channel`);

        return [this.form];
    }

    /**
     * @returns {Promise<SmartFormElement>}
     *
     * @private
     */
    async _createDeviceSelect() {
        const clients = await this.apiGateway.getClients();

        return SelectInputTag('application', clients.list)`Application`;
    }

    /**
     * @param {{closeModal: function}} detail
     *
     * @private
     */
    async _onModalSubmit({ detail }) {
        try {
            const response = await this._putDevice(this.form.getData());

            this.messageContainer.dispatchCustomEvent('smartMessage', { messages: response.message });
        } catch (e) {
            this.messageContainer.dispatchCustomEvent('smartMessage', { messages: e });
        }

        detail.closeModal();
        const component = document.createElement(DevicesView.tagName);
        component.room = this.room;

        this.window.dispatchCustomEvent('loadNewRoot', {
            component,
        });
    }

    /**
     * @param {string} name
     * @param {string} channel
     * @param {string} application
     * @returns {Promise<object>}
     *
     * @private
     */
    async _putDevice({ name, channel, application }) {
        const response = await this.apiGateway.create(
            apiUrl`room/${this.room}/device/${name}`,
            { application, channel },
        );

        if (!response.success && !response.response) {
            throw response.message || [{ error: 'Could not create device' }];
        }

        return response || {};
    }
}

export const AddDeviceModalViewTag = AddDeviceModalView.register();
