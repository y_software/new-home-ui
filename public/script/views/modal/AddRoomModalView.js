import { ApiGateway, apiUrl } from '../../service/ApiGateway.js';
import { RoomsView } from '../RoomsView.js';
import { SmartFormTag } from '../../element/form/SmartForm.js';
import { SmartHTMLElement } from '../../element/SmartHTMLElement.js';
import { TextInputTag } from '../../element/form/TextInput.js';

export class AddRoomModalView extends SmartHTMLElement {
    apiGateway = new ApiGateway();

    /**
     * @type {SmartForm}
     */
    form = SmartFormTag``;

    messageContainer = document.getElementById('messages');

    /**
     * @type {SmartHTMLElement}
     */
    window;

    getContent() {
        this.parentElement.addEventListener('SmartModal.submit', this._onModalSubmit.bind(this));

        this.form.addInput(TextInputTag('name')`Name`);

        return [this.form];
    }

    /**
     * @param {{closeModal: function}} detail
     *
     * @private
     */
    async _onModalSubmit({ detail }) {
        try {
            const response = await this._putRoom(this.form.getData());

            this.messageContainer.dispatchCustomEvent('smartMessage', { messages: response.message });
        } catch (e) {
            this.messageContainer.dispatchCustomEvent('smartMessage', { messages: e });
        }

        detail.closeModal();
        this.window.dispatchCustomEvent('loadNewRoot', {
            component: document.createElement(RoomsView.tagName),
        });
    }

    /**
     * @param {string} name
     * @returns {Promise<object>}
     * @private
     */
    async _putRoom({ name }) {
        const response = await this.apiGateway.create(apiUrl`room/${name}`);

        if (!response.success && !response.response) {
            throw response.message || [{ error: 'Could not create room' }];
        }

        return response || {};
    }
}

export const AddRoomModalViewTag = AddRoomModalView.register();
