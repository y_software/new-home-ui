import { ApplicationWindow } from '../element/ApplicationWindow.js';
import { ApplicationsView } from './ApplicationsView.js';
import { Language } from '../service/LanguageLoader.js';
import { RoomsView } from './RoomsView.js';
import { SmartButtonTag } from '../element/SmartButton.js';

export class DashboardView extends ApplicationWindow {
    hash = 'dashboard';

    trustedHashes = [''];

    async getWindowComponents() {
        const rooms = SmartButtonTag`${await Language.index_navigation_rooms}`;
        rooms.addEventListener('click', this.loadView.bind(this, RoomsView));

        const applications = SmartButtonTag`${await Language.index_navigation_applications}`;
        applications.addEventListener('click', this.loadView.bind(this, ApplicationsView));

        return [
            rooms,
            applications,
        ];
    }

    loadView(view) {
        this.dispatchCustomEvent('loadNewRoot', { component: document.createElement(view.tagName) });
    }
}

DashboardView.register();
