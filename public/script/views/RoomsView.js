import { ApiGateway, apiUrl } from '../service/ApiGateway.js';
import { AddRoomModalView } from './modal/AddRoomModalView.js';
import { ApplicationWindow } from '../element/ApplicationWindow.js';
import { DevicesView } from './DevicesView.js';
import { Div } from '../functions/Html.js';
import { Language } from '../service/LanguageLoader.js';
import { SmartButtonTag } from '../element/SmartButton.js';
import { SmartModalTag } from '../element/SmartModal.js';
import { SmartTitleTag } from '../element/SmartTitle.js';

const ROOMS_MIN_LENGTH = 1;

export class RoomsView extends ApplicationWindow {
    apiGateway = new ApiGateway();

    hash = 'rooms';

    getStyles() {
        return `${super.getStyles()}
            .button--add {
                margin-left: 1em;
            }

            .title-line {
                align-items: center;
                display: flex;
                width: 100%;
            }

            .button--add, .button--remove {
                background: none;
                border: none;
                color: var(--colors--message--success);
                font-weight: bold;
                height: 1.2em;
                font-size: 1.5em;
                padding: 0;
                width: 1.2em;
            }

            .button--remove {
                color: var(--colors--message--error);
                position: absolute;
                top: 0.5em;
                left: 1em;
            }

            .room-button {
                position: relative;
            }
        `;
    }

    async getWindowComponents() {
        const rooms = await this._getRooms();
        const addButton = document.createElement('button');
        addButton.innerHTML = '+';
        addButton.classList.add('button--add');
        addButton.addEventListener('click', this._onClickAddRoom.bind(this));

        const titleLine = Div`<smart-title>${await Language.view_rooms_title}</smart-title>${addButton}`;
        titleLine.classList.add('title-line');
        titleLine.classList.add('grid-container--full-width');

        return [
            titleLine,
            ...await this._getRoomButtons(rooms),
        ];
    }

    /**
     * This method cannot be called in the context of the room overview because
     * there is not one single application registered for all rooms.
     *
     * @param {string} methodName
     * @param {object} [body]
     *
     * @returns {Promise<{}>}
     */
    async callOwnMethod(methodName, body) {
        return {
            body,
            methodName,
        };
    }

    /**
     * @param {string[]} rooms
     * @returns {Promise<HTMLElement[]>}
     *
     * @private
     */
    async _getRoomButtons(rooms) {
        if (rooms.length < ROOMS_MIN_LENGTH) {
            return [SmartTitleTag.center`${await Language.view_rooms_message_no_room}`];
        }

        return rooms.map(this._createRoomButton.bind(this));
    }

    /**
     * @returns {Promise<string[]>}
     *
     * @private
     */
    async _getRooms() {
        const response = await this.apiGateway.getRooms();

        if (!response.success && response.message) {
            this.addMessages(response.message);
        }

        return response.list || [];
    }

    /**
     * @param room
     * @returns {HTMLElement}
     *
     * @private
     */
    _createRoomButton(room) {
        const removeRoomButton = document.createElement('button');
        removeRoomButton.innerHTML = '×';
        removeRoomButton.classList.add('button--remove');
        removeRoomButton.addEventListener('click', this._onRemoveAddRoom.bind(this, room));

        const roomButton = SmartButtonTag`${room}${removeRoomButton}`;
        roomButton.addEventListener('click', this._loadRoom.bind(this, room));

        return Div.options({ classList: ['room-button'] })`${roomButton}${removeRoomButton}`;
    }

    /**
     * @param {string} room
     *
     * @private
     */
    _loadRoom(room) {
        const devicesView = document.createElement(DevicesView.tagName);
        devicesView.room = room;

        this.dispatchCustomEvent('loadNewRoot', { component: devicesView });
    }

    async _onClickAddRoom() {
        const modalView = document.createElement(AddRoomModalView.tagName);
        modalView.window = this;

        const modal = SmartModalTag`${modalView}`;
        modal.title = await Language.modal_add_room_title;
        modal.closeText = await Language.modal_button_close;

        document.body.appendChild(modal);
    }

    async _onRemoveAddRoom(room) {
        if (!confirm(`${await Language.view_rooms_message_remove_room} ${room}`)) {
            return;
        }

        const response = await this.apiGateway.remove(apiUrl`room/${room}`);

        this.dispatchCustomEvent('loadNewRoot', {
            component: document.createElement(RoomsView.tagName),
        });

        if (!response.success && !response.response) {
            this.addMessages(response.message || [{ error: await Language.view_rooms_message_error_create }]);

            return;
        }

        this.addMessages(response.message);
    }
}

export const RoomsViewTag = RoomsView.register();
