import { ApiGateway, apiUrl } from '../service/ApiGateway.js';
import { AddDeviceModalView } from './modal/AddDeviceModalView.js';
import { ApplicationWindow } from '../element/ApplicationWindow.js';
import { DeviceActionButtonTag } from '../element/DeviceActionButton.js';
import { Div } from '../functions/Html.js';
import { Language } from '../service/LanguageLoader.js';
import { SmartModalTag } from '../element/SmartModal.js';
import { SmartTitleTag } from '../element/SmartTitle.js';

const DEVICES_MIN_LENGTH = 1;

export class DevicesView extends ApplicationWindow {
    apiGateway = new ApiGateway();

    room = '';

    connectedCallback() {
        this.hash = `rooms/${this.room}/devices`;

        super.connectedCallback();
    }

    getStyles() {
        return `${super.getStyles()}
            .button--add {
                margin-left: 1em;
            }

            .title-line {
                align-items: center;
                display: flex;
                width: 100%;
            }

            .button--add, .button--remove {
                background: none;
                border: none;
                color: var(--colors--message--success);
                font-weight: bold;
                height: 1.2em;
                font-size: 1.5em;
                padding: 0;
                width: 1.2em;
            }
            
            .button--remove {
                color: var(--colors--message--error);
                position: absolute;
                top: 0.5em;
                left: 1em;
            }
            
            .device-button {
                position: relative;
            }
        `;
    }

    /**
     * This method cannot be called in the context of a room because
     * there is not one single application registered for the room.
     *
     * @param {string} methodName
     * @param {object} [body]
     *
     * @returns {Promise<{}>}
     */
    async callOwnMethod(methodName, body) {
        return {
            body,
            methodName,
        };
    }

    async getWindowComponents() {
        const devices = await this._getDevices();
        const addButton = document.createElement('button');
        addButton.innerHTML = '+';
        addButton.classList.add('button--add');
        addButton.addEventListener('click', this._onClickAddDevice.bind(this));

        const titleLine = Div`<smart-title>${await Language.view_devices_title} ${this.room}</smart-title>${addButton}`;
        titleLine.classList.add('title-line');
        titleLine.classList.add('grid-container--full-width');

        return [
            titleLine,
            ...await this._getDeviceButtons(devices),
        ];
    }

    /**
     * @param {{[string]: {application: string, channel: string}}} devices
     * @returns {Promise<HTMLElement[]>}
     *
     * @private
     */
    async _getDeviceButtons(devices) {
        if (Object.keys(devices).length < DEVICES_MIN_LENGTH) {
            return [SmartTitleTag.center`${await Language.view_devices_message_no_devices}`];
        }

        const buttons = [];

        for (const [device, config] of Object.entries(devices)) {
            buttons.push(this._createDeviceButton(device, config));
        }

        return buttons;
    }

    /**
     * @returns {Promise<{[string]: {application: string, channel: string}}>}
     *
     * @private
     */
    async _getDevices() {
        return (await this.apiGateway.getDevices(this.room)).table || {};
    }

    /**
     * @param {string} device
     * @param {{application: string, channel: string}} config
     * @returns {HTMLElement}
     *
     * @private
     */
    _createDeviceButton(device, config) {
        const removeDeviceButton = document.createElement('button');
        removeDeviceButton.innerHTML = '×';
        removeDeviceButton.classList.add('button--remove');
        removeDeviceButton.addEventListener('click', this._onRemoveDevice.bind(this, device));

        const deviceButton = DeviceActionButtonTag(config.application, config.channel)`${device}`;

        return Div.options({ classList: ['device-button'] })`${deviceButton}${removeDeviceButton}`;
    }

    async _onClickAddDevice() {
        const modalView = document.createElement(AddDeviceModalView.tagName);
        modalView.window = this;
        modalView.room = this.room;

        const modal = SmartModalTag`${modalView}`;
        modal.title = `${await Language.modal_add_device_title} ${this.room}`;
        modal.closeText = await Language.modal_button_cancel;

        document.body.appendChild(modal);
    }

    async _onRemoveDevice(device) {
        if (!confirm(`${await Language.view_devices_message_remove_device} ${device}`)) {
            return;
        }

        const response = await this.apiGateway.remove(apiUrl`room/${this.room}/device/${device}`);

        const component = document.createElement(DevicesView.tagName);
        component.room = this.room;
        this.dispatchCustomEvent('loadNewRoot', { component });

        if (!response.success && !response.response) {
            this.addMessages(response.message || [{ error: await Language.view_devices_message_error_create }]);

            return;
        }

        this.addMessages(response.message);
    }
}

export const DevicesViewTag = DevicesView.register();
