export class NavigationItem {
    /**
     * @param {string} title the text inside the navigation item
     * @param {function} onClick a callable that is triggered when the menu item is clicked
     */
    constructor(title, onClick) {
        this.title = title;
        this.onClick = onClick;
    }
}
