let appCache = null;

let settings = {
    // This is used to keep the naming of the settings consistent.
    // eslint-disable-next-line camelcase
    connection_type: '',

    // eslint-disable-next-line camelcase
    proxy_password: '',

    // eslint-disable-next-line camelcase
    proxy_username: '',
};

const cachePatterns = [
    new RegExp('.*\\.js$'),
    new RegExp('.*\\.json$'),
    new RegExp('.*\\.html$'),
    new RegExp('.*\\.css$'),
    new RegExp('.*\\.png$'),
    new RegExp('.*\\.svg$'),
    new RegExp('.*\\.jpe?g$'),
    new RegExp('^/$'),
];

/**
 * @returns {Promise<Cache>}
 */
async function getAppCache() {
    if (!appCache) {
        appCache = await caches.open('new-home-ui');
    }

    return appCache;
}

/**
 * @param {string} url
 *
 * @returns {boolean}
 */
async function shouldCache(url) {
    return (
        await Promise.all(cachePatterns.map((regex) => url.match(regex) !== null))
    ).reduce((last, current) => last && current);
}

/**
 * @param {Request} request
 * @returns {Request}
 */
function createWebRequest(request) {
    const additionalHeaders = {};

    if (settings.connection_type === 'Proxy') {
        additionalHeaders.Authorization = `Basic ${btoa(`${settings.proxy_username}:${settings.proxy_password}`)}`;
    }

    return new Request(request, {
        headers: {
            ...request.headers,
            ...additionalHeaders,
        },
    });
}

/**
 * @param {Request} request
 *
 * @returns {Promise<Response>}
 */
async function getResponse(request) {
    const cache = await getAppCache();
    const webRequest = createWebRequest(request);

    if (!await shouldCache((new URL(request.url)).pathname)) {
        return fetch(webRequest);
    }

    let response = await cache.match(request);

    if (response) {
        return response;
    }

    response = await fetch(webRequest);

    // This console has some serious information, so it stays!
    // eslint-disable-next-line no-console
    cache.put(request, response.clone()).catch(console.error);

    return response;
}

/**
 * @param {FetchEvent} event
 */
function onFetch(event) {
    event.respondWith(getResponse(event.request));
}

/**
 * @param {object} data
 */
function onMessage({ data }) {
    if (data.settings) {
        settings = data.settings;
    }
}

self.addEventListener('fetch', onFetch.bind(self));
self.addEventListener('message', onMessage.bind(self));
