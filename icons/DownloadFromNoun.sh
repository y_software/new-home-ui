#!/usr/bin/env bash

set -e

COLLECTION="${1}"
ICON_REQUEST_JSON_STRING='{"operationName":"IconDetailQuery","variables":{"iconId":"${ICON}"},"query":"query IconDetailQuery($iconId: ID) {\n  iconDetail(id: $iconId) {\n    id\n    isActive\n    name\n    term\n    permalink\n    tags {\n      id\n      slug\n      __typename\n    }\n    collections {\n      id\n      name\n      slug\n      permalink\n      __typename\n    }\n    icon_url: iconUrl\n    license_description: licenseDescription\n    uploader {\n      id\n      name: fullName\n      username\n      permalink\n      location\n      __typename\n    }\n    userHasPurchased\n    attributionPreviewUrl\n    preview_url: thumbnail\n    __typename\n  }\n}\n"}'

function download_icon() {
  ICON="${1}"
  THIS_REQUEST="$(echo "${ICON_REQUEST_JSON_STRING}" | sed -r 's/\$\{ICON\}/'"${ICON}"'/')"

  RESPONSE=$(curl 'https://api.production.thenounproject.com/graphql' \
    -H 'authority: api.production.thenounproject.com' \
    -H 'pragma: no-cache' \
    -H 'cache-control: no-cache' \
    -H 'accept: */*' \
    -H 'origin: https://thenounproject.com' \
    -H 'sec-fetch-dest: empty' \
    -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36' \
    -H 'dnt: 1' \
    -H 'content-type: application/json' \
    -H 'sec-fetch-site: same-site' \
    -H 'sec-fetch-mode: cors' \
    -H 'referer: https://thenounproject.com/smashicons/collection/households-solid/?i=276471' \
    -H 'accept-language: de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7' \
    -H 'cookie: __stripe_mid=f639621d-1a6b-4b38-b471-ade07f7a2394; __stripe_sid=18b1fed9-1edc-40c4-a7f5-2c6e5116ea0c; csrftoken=ltCmoyTz6fWv1Vzjn8Bk8u5rtXrK6ueTGyrhszOeician6AMFVSgE8ngoh5s40L8; sessionid=ssibid5zcqgdp4awbn26xjthywx1esh5' \
    --data-binary "${THIS_REQUEST}" --compressed)

  IMAGE_URL=$(echo "${RESPONSE}" | jq .data.iconDetail.icon_url -r)

  wget "${IMAGE_URL}" -O "${ICON}.svg"
}

for LINE in $(cat icons.list); do
  download_icon "${LINE%}"
done
